package p1;

import java.util.Collections;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.printf("Start Interval:  ");
		int startInterval = in.nextInt();
		System.out.printf("End Interval:  ");
		int endInterval = in.nextInt();
		
		PrimeService prime = new PrimeService();
		prime.bigPrime(startInterval,endInterval);
		int max = Collections.max(prime.sumTerms);
		int indexMax = prime.sumTerms.indexOf(max);		
		
		System.out.println("Largest prime: "+prime.sumList.get(indexMax));
		System.out.println("Number of terms: "+ max);
		in.close();
		
	}
}