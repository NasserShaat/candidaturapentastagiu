package p1;

import java.util.ArrayList;
import java.util.List;

public class PrimeService {

	public List<Integer> numbersList = new ArrayList<Integer>();//list with prime numbers
	public List<Integer> sumTerms = new ArrayList<Integer>();//list with no of terms
	public List<Integer> sumList = new ArrayList<Integer>(); //list with different sums 
	
	public void bigPrime(int startInterval, int endInterval) 
	{
		if (endInterval > 1 ) 
		{
			int sumPrime = 0;
			int newIndex,counter; 
			for (int i = 0; i <= endInterval; i++) 
			{
				if (PrimeChecker.isPrime(i)) 
				{
					numbersList.add(i);//add to the list with prime numbers
				}
			}
			
			for (int i = 0; i < numbersList.size(); i++) // from firstPrime until last prime from the list with prime numbers
			{
				newIndex = 0;
				for (int j = 0; j < i + 1; j++)//from first index from the list with prime numbers until firstPrime
				{
					newIndex = j;
					sumPrime = 0;
					counter = 0;
					while (sumPrime < numbersList.get(i))
					{
						sumPrime += numbersList.get(newIndex);
						newIndex++;
						counter++;//increment the counter with terms of sum
					}
					if (sumPrime == numbersList.get(i)) 
					{
						sumList.add(sumPrime);
						sumTerms.add(counter);
					}
				}
			}
		} 
		else 
		{
			System.err.println("No prime numbers in this interval!");
		}
	}
}
